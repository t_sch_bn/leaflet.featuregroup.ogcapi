
## v0.1.0 (2022-07-14)

* Initial release
* Part of the OGC codesprint held on 2022-07-12 to 2022-07-14
* Implemented Part 1: loads features in the visible bbox
* Partially implemented Part 4: functionalities for creating, replacing and deleting (but not updating) features

